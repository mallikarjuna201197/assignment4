#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @tag1
  Scenario: Title of your scenario
    Given the url http://spezicoe.wipro.com:81/opencart1/ is launched
	When the user clicks on My Account and Login
	And the user enters the Email and password
	And clicks the Login button
	And clicks on store link available on top left corner
	Then Home page should be displayed
	And clicks on Brands link available on bottom navbar under Extras
	Then Find your favourite Brand page should be displayed
	And User should be able to logout
    